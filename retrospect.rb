#! /usr/bin/env ruby -w

SECONDS = 10
OUTPUT = "/tmp/movie.mov"
BASEDIR = File.dirname(File.realpath(__FILE__))
COMPNAME = `/usr/sbin/networksetup -getcomputername`

trap(:INT) do
    puts 'This is the first signal handler'
    exit
end

i = 0
while true do
    outfile = "/tmp/img_#{COMPNAME}_#{i}.jpg"
    system "/usr/sbin/screencapture", '-t', 'jpg', outfile
    i = i + 1
    sleep 1
end
